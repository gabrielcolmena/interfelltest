//
//  Constants.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 4/30/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import Foundation

var __iconsFont = "ionicons"
var __mainStoryboardID = "Main"
var __registersVCID = "RegistersVC"
var __requiredFieldsMessage = "Todos los campos son requeridos"
var __acceptTitleButton = "Aceptar"
var __resultTitleButton = "Resultado"
var __retultStringTemplate = "%@ \n IMC calculado: %@ "
