//
//  UINavigationControllerExt.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 4/30/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    var isNavigationBaTransparent: Bool {
        get{
            return self.isNavigationBaTransparent
        }
        set {
            if newValue {
                navigationBar.setBackgroundImage(UIImage(), for: .default)
                navigationBar.shadowImage = UIImage()
                navigationBar.isTranslucent = true
                view.backgroundColor = .clear
            }
        }
    }
    
}
