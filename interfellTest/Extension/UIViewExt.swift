//
//  UIViewExt.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 4/30/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import UIKit
private var shadowKey = false

extension UIView {
    
    @IBInspectable var shadow: Bool{
        get{ return shadowKey }
        set{
            shadowKey = newValue
            
            if shadowKey{
                self.layer.masksToBounds = false
                self.layer.shadowColor = UIColor.black.cgColor
                self.layer.shadowOpacity = 0.2
                self.layer.shadowOffset = CGSize(width: -2, height: 2)
                self.layer.shadowRadius = 5
            }else{
                self.layer.masksToBounds = true
            }
        }
    }
    
}


extension UIViewController {
    
    var hideKeyboardOnTap: Bool{
        set {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
            if newValue{
                view.addGestureRecognizer(tap)
            } else {
                view.removeGestureRecognizer(tap)
            }
        }
        get{
            return self.hideKeyboardOnTap
        }
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
