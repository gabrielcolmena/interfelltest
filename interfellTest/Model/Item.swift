//
//  Item.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 5/1/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import Foundation
import Firebase

struct ItemJSONKeys{
    static let age = "age"
    static let height = "height"
    static let weight = "weight"
    static let gender = "gender"
    static let cat = "cat"
}

class Item{
    
    var age: Int?
    var height: Double?
    var weight: Double?
    var gender: String?
    var cat: String?
    var key: String?
    
    var formatedAge: String? {
        return "\(self.age ?? 0) años"
    }
    
    var imc: Double? {
        guard let weight = weight, let height = height else { return 0.0 }
        return weight/(pow(height, Double(2)))
    }
    
    init(snap: [String: Any], key: String){
        self.key = key
        if let age = snap[ItemJSONKeys.age] as? Int{
            self.age = age
        }
        
        if let height = snap[ItemJSONKeys.height] as? Double{
            self.height = height
        }
        
        if let weight = snap[ItemJSONKeys.weight] as? Double{
            self.weight = weight
        }
        
        if let gender = snap[ItemJSONKeys.gender] as? String{
            self.gender = gender
        }
        
        if let cat = snap[ItemJSONKeys.cat] as? String{
            self.cat = cat
        }
    }
    
    init(age: Int, height: Double, weight: Double, gender: String, cat: String){
        self.key = ""
        self.age = age
        self.height = height
        self.weight = weight
        self.gender = gender
        self.cat = cat
    }
}
