//
//  RegistersCell.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 5/1/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import UIKit

class RegistersCell: UITableViewCell {

    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    static let ID = "RegistersCell"

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
