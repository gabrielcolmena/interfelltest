//
//  RegisterVC.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 5/1/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import UIKit

class RegistersVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var presenter: RegistersPresenter?
    
    var items = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 60
        self.tableView.register(UINib(nibName: RegistersCell.ID, bundle: nil), forCellReuseIdentifier: RegistersCell.ID)
        
        presenter = RegistersPresenter(view: self)
    }

}

extension RegistersVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RegistersCell.ID) as! RegistersCell
        let item = items[indexPath.row]
        cell.resultLabel.text = item.cat
        cell.ageLabel.text = item.formatedAge
        cell.sexLabel.text = item.gender
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let item = items[indexPath.row]
            presenter?.removeValue(item: item)
            self.items.remove(at: indexPath.row)
            tableView.reloadData()
        }
    }
}

extension RegistersVC: RegistersView{
    func setNewItem(item: Item) {
        items.append(item)
        tableView.reloadData()
    }
}
