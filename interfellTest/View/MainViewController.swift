//
//  ViewController.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 4/30/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var ageField: UITextField!
    @IBOutlet weak var sexField: UITextField!
    @IBOutlet weak var heightField: UITextField!
    @IBOutlet weak var weightField: UITextField!
    
    var presenter: MainViewPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MainViewPresenter(view: self)
        hideKeyboardOnTap = true
        ageField.keyboardType = .numberPad
        heightField.keyboardType = .decimalPad
        weightField.keyboardType = .decimalPad
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = true
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
        UIApplication.shared.statusBarStyle = .default
    }
    
    @IBAction func moveToRegisters(_ sender: UIButton){
        let Storyboard = UIStoryboard(name: __mainStoryboardID, bundle: nil)
        let vc = Storyboard.instantiateViewController(withIdentifier: __registersVCID)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func calcButtonPressed(_ sender: UIButton){
        guard !(ageField.text?.isEmpty)! ,
            !(sexField.text?.isEmpty)!,
            !(heightField.text?.isEmpty)!,
            !(weightField.text?.isEmpty)! else { return showValidationAlert() }
        
        let result = presenter?.calculateButtonTapped(height: heightField.text, weight: weightField.text, age: ageField.text, gender: sexField.text)
        let alert = UIAlertController(title: __resultTitleButton, message: result, preferredStyle: .alert)
        let action = UIAlertAction(title: __acceptTitleButton, style: .default)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        cleanTextFields()
    }
    
    func showValidationAlert(){
        let alert = UIAlertController(title: "", message: __requiredFieldsMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: __acceptTitleButton, style: .default)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    func cleanTextFields(){
        ageField.text = ""
        sexField.text = ""
        heightField.text = ""
        weightField.text = ""
    }
}

extension MainViewController: MainView{
    
}
