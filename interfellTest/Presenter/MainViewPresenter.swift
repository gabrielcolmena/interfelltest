//
//  MainViewPresenter.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 4/30/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import Foundation
import UIKit

protocol MainView: NSObjectProtocol {
    
}

class MainViewPresenter {
    
    var view: MainView
    init(view: MainView){
        self.view = view
        
    }
    
    func calculateButtonTapped(height: String?, weight: String?, age: String?, gender: String? ) -> String {
        var category = ""
        var result: Double = 0.0
        if let numHeight = Double(height ?? ""),
            let numWeight = Double(weight ?? ""),
            let numAge = Int(age ?? ""){
            result = numWeight/(pow(numHeight, Double(2)))
            
            if result < 18.0 {
                category = "Peso bajo"
            } else if result >=  18, result < 25 {
                category = "Peso Normal"
            } else if result >= 25, result < 27 {
                category = "Sobrepeso"
            } else if result >= 27, result < 30 {
                category = "Obesidad grado 1"
            } else if result >= 30, result < 40 {
                category = "Obesidad grado 2"
            } else if result >= 40 {
                category = "Obesidad grado 3, extrema o mórbida"
            }
            
            let item = Item(age: numAge, height: numHeight, weight: numWeight, gender: gender ?? "", cat: category)
            FirebaseServices.setNewValue(item: item)
            
        }
        let formatedResult = String(format: "%.2f", result)
        return String(format: __retultStringTemplate, arguments: [formatedResult, category ])
    }
}
