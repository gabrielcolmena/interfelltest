//
//  RegistersPresenter.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 5/1/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import Foundation

protocol RegistersView: NSObjectProtocol{
    func setNewItem(item: Item)
}


class RegistersPresenter{
    let view: RegistersView?
    
    init(view: RegistersView){
        self.view = view
        getData()
    }
    
    func getData(){
        FirebaseServices.getAllData(completion: { item in
            guard let item = item else { return }
            
            self.view?.setNewItem(item: item)
        })
    }
    
    
    func removeValue(item: Item){
        FirebaseServices.removeValue(byKey: item.key ?? "")
    }
}
