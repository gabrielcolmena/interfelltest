//
//  FirebaseSerivces.swift
//  interfellTest
//
//  Created by Gabriel Colmenares on 5/1/18.
//  Copyright © 2018 Gabriel Colmenares. All rights reserved.
//

import FirebaseDatabase
import Foundation
import Firebase



class FirebaseServices {
    static func getAllData(completion: @escaping (Item?) -> Void){
        let dbRef = Database.database().reference().child("logs")
        dbRef.observeSingleEvent(of: .value, with: { snapShots in
            for snap in snapShots.children {
                guard let snap = snap as? DataSnapshot else { return completion(nil) }
                if let dict = snap.value as? [String: Any]{
                    let item = Item(snap: dict, key: snap.key)
                    completion(item)
                }
            }
        })
    }
    
    static func removeValue(byKey: String){
        let dbRef = Database.database().reference().child("logs").child(byKey)
        dbRef.removeValue()
    }
    
    static func setNewValue(item: Item){
        let dbRef = Database.database().reference().child("logs")
        
        let updateObject: [String: Any] = [
            "height": item.height ?? 0.0,
            "weight": item.weight ?? 0.0,
            "gender": item.gender ?? "",
            "age": item.age ?? 0,
            "cat": item.cat ?? ""
        ]
        
        let id = dbRef.childByAutoId().key
        dbRef.child(id).updateChildValues(updateObject)
    }
}
